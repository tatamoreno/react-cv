import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './Education.scss';

class Education extends Component{
    render(){
        return(
            <div className='education'>
                <h3>Education</h3>
                <ul> {this.props.education.map(el =>{
                        return (
                            <li key={el.title}>
                                <p>{el.period}: {el.title} - {el.company}</p>
                            </li>
                        );
                    })}
                </ul>
            </div>
        )
    }
}

Education.propTypes = {
	elements: PropTypes.arrayOf(PropTypes.shape({
		period: PropTypes.string,
		title: PropTypes.string,
		company: PropTypes.string,
	}))
}

Education.defaultProps = {
	elements: []
}

export default Education;