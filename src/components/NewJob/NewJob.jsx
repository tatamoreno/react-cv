import React, {Component} from 'react';
import './NewJob.scss';

class NewJob extends Component{

    render(){

        const {period, title, company} = this.props.newJob;
        return(
            <ul>
                <li>{period}: {title} - {company}</li>
            </ul>
        )
    }
}

export default NewJob;