import React, {Component} from 'react';
import NewJobForm from '../../containers/NewJobForm';
import NewJob from '../../components/NewJob';
import './Jobs.scss';

class Jobs extends Component{

    state = {
        newJobsList: []
    };

    addJobs = newJob => {

        this.setState({ 
            newJobsList: [...this.state.newJobsList, newJob]})

    }

    render(){

        return(
            <div>
                <div className='Jobs'>
                    <h3>Jobs</h3>
                    <div>
                        {this.state.newJobsList.map(newJob => {
                            return(
                                <NewJob key={`${newJob.period}: ${newJob.title} - ${newJob.company}`} newJob = {newJob} />
                            );
                        })}
                    </div>

                    <ul> {this.props.alljobs.map(el =>{
                            return (
                                <li key={el.title}>
                                    <p>{el.period}: {el.title} - {el.company}</p>
                                </li>
                            );
                        })}
                    </ul>
                </div>
                <NewJobForm createJob={this.addJobs}/>
            </div>
        )
    }
}

export default Jobs;