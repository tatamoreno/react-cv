import React, {Component} from 'react';
import './Header.scss';

class Header extends Component{
    render(){
        return(
            <div className="container">
                <div className="container__info">
                    <div className="container__info-bigfont">{this.props.fullname}</div>
                    <h3>{this.props.slogan}</h3>
                    <p>{this.props.email}</p>
                </div>
                <div className="container__photo">
                    <img src={this.props.image} alt="profilePic"/>
                </div>
            </div>
        );
    }
}

export default Header;