import './App.scss';
import React, {Component} from 'react';
import Header from './components/Header';
import Education from './components/Education';
import Jobs from './components/Jobs';
import NewJobForm from './containers/NewJobForm';

class App extends Component {

  state = { 
    user:{
	  name: 'Tatiana ',
	  surname: 'Moreno',
    image: '/profilepic.jpg',
    slogan: 'Programadora web apasionada con la creatividad',
    email: 'tatianamoreno.mon@gmail.com',
	  education: [
      { period: "2020 - 2021", title: 'Bootcamp Full Stack Developer', company: 'Upgrade-Hub' },
      { period: "2015 - 2017", title: 'Lenguas modernas con énfasis en TICS', company: 'Escuela de administración de negocios.' },
	  	{ period: "2013 - 2014", title: 'Técnico laboral por competencias Diseño gráfico y Pre-prensa digital para 	medios impresos', company: 'SENA' },
      { period: "2011 - 2013", title: 'General English Course', company: 'Universidad Sergio Arboleda' },
	  ],
	  jobs: [
      { period: "2020", title: 'Teleoperadora', company: 'INDRA' },
      { period: "2019 - 2020", title: 'Asistente de organización de eventos', company: 'Laura’s Bridal' },
      { period: "2016 - 2019", title: 'Diseñador gráfico, actualizador de contenidos', company: 'Colombia Webmasters' },
      { period: "2015 - 2016", title: 'Agente de servicio al cliente bilingüe', company: 'A&E FACTORY SERVICES, SITEL' },
	  ],
  }};

  render(){

    const {user} = this.state;

    return(

      <div className='App'>
        <div className="App__header">
          <Header fullname={user.name + user.surname} image={user.image} slogan={user.slogan} email={user.email}/>
        </div>
        <div className="App__content">
          <Education education={user.education}/>
          <Jobs alljobs={user.jobs}>
            <NewJobForm/>
          </Jobs>
        </div>
      </div>
    );
  }
}

export default App;