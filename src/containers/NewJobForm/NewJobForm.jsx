import React, {Component} from 'react';
import './NewJobForm.scss';

const INITIAL_FORM = {

    period:'',
    title:'',
    company:'',
}

class NewJobForm extends Component{

    state = INITIAL_FORM;

    handleFormSubmit = ev => {
        ev.preventDefault();
        this.props.createJob(this.state)
        this.setState(INITIAL_FORM);
    }

    onChangeInput = ev =>{
        const { name, value} = ev.target;
        this.setState({ [name]: value});

    }

    render(){
        return(
            <div>
                <form className="NewJobForm" onSubmit={this.handleFormSubmit}>
                    <h3>Add jobs</h3>
                    <label>
                        <p>Period:</p>
                        <input type="text" name="period" value={this.state.period} onChange={this.onChangeInput}/>
                    </label>
                    <label>
                        <p>Title:</p>
                        <input type="text" name="title" value={this.state.title} onChange={this.onChangeInput}/>
                    </label>
                    <label>
                        <p>Company:</p>
                        <input type="text" name="company" value={this.state.company} onChange={this.onChangeInput}/>
                    </label>
                    <div>
                    <input className="NewJobForm__button" type="submit" value="Guardar"/>
                    </div>
                </form>
            </div>
        );
    }
}

export default NewJobForm;