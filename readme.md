# Ejercicio de clase, crear tu CV con React

En este ejercicio, vamos a crear una aplicación simple con React para mostrar la información de tu CV.

### Fechas:
- Entrega: 23 de Enero. Deberás tener el ejercicio subido y actualizado. El mismo día 23 lo revisaremos en clase y veremos dudas que hayan surgido.
- Entrega Final: 26 de Enero. Después de resolver las dudas, la entrega final será el Martes 26 en clase.
- Este ejercicio cuenta como 1 punto del proyecto final.

Vamos a trabajar props, state, componentes, containers y logica de aplicación.

1. Clonar este repositorio
2. Una vez clonado, desde la consola tienes que sacar una rama con tu nombre y apellido. Todo el código que hagas, será dentro de tu rama. Usaremos el siguiente comando: `git checkout -b nombre-apellido`
3. Dentro de esa rama, tendrás una copia del proyecto. con react instalado.
4. Instala dependencias `npm i`
5. Limpia React del contenido que trae por defecto para tener nuestro proyecto limpio (logo, código inicial etc...)
6. Programa tu código.
7. Tienes que ir haciendo commits mientras programas
8. Una vez terminado, sube el código al repositorio con el comando `git push`.
9. Al ser una nueva rama, te pedirá que introduzcas el siguiente comando para subirla `git push --set-upstream origin tunombre-tuapellido` (puedes copiar este comando desde la consola si has ejecutado `git push`)
10. Avísame por Teams enviándome un mensaje (Juan Macías) de que has terminado el ejercicio.

## La estructura del ejercicio es la siguiente:

El componente App:
* Tiene 3 hijos: Education, Header y Jobs.
* Almacena en su estado todos los datos del CV.
* A education le pasa como prop, solamente los datos de education.
* A header le pasa tu nombre y apellido y tu profesión
* A Jobs le pasa tus trabajos.


El componente Header
* No tiene lógica, solo renderiza los datos que recibe.

El componente Jobs:
* Recibe los datos como props.
* Este componente renderizará otros dos componentes más: `Job` y `NewJobForm`
    * Job: Renderiza un trabajo.
    * NewJobForm. Es un formulario con 3 inputs y un botón submit. Este formulario al enviarse debe de añadir un nuevo trabajo y debe de actualizar el estado de App con este nuevo trabajo.

El componente Education:
* Renderiza un listado de EducationItem, cada uno de estos renderiza un único puesto de trabajo.




![exercise](https://i.ibb.co/hZB4qHJ/react-cv-exercise.jpg)
